import sys, os, re
from operator import itemgetter, methodcaller

players = dict()

def insertPlayerStats(name, batting_times, hits, runs):
	name = ""+name
	name = ""+name
	if not name in players:
		players[name] = (0,0,0)
	players[name] = tuple(map(sum,zip(players[name], (batting_times, hits, runs))))

if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])
	
filename = sys.argv[1]

if not os.path.exists(filename):
	sys.exist("Error: File '%s' not found" % sys.argv[1])
	
f = open(filename)



header_regex = re.compile(r"^===");
player_regex = re.compile(r"(\w+ \w+) batted (\d+) times with (\d+) hits and (\d) runs");
for line in f:
	match = header_regex.match(line.rstrip())
	if match is not None:
		continue
	match = player_regex.match(line.rstrip())
	if match is not None:
		insertPlayerStats(match.group(1), float(match.group(2)), float(match.group(3)), float(match.group(4)))

def getAverage(player):
	return (player, players[player][1]/players[player][0])

averages = list(map(getAverage, players))

averages.sort( key=lambda player: player[1], reverse=True)

for player in averages:
	print("%s: %.3f" % (player[0], player[1])) 
 
f.close()

